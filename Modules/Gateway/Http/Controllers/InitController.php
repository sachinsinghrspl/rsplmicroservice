<?php

namespace Modules\Gateway\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use GuzzleHttp\Exception\ClientException;
use Config;

class InitController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request, $uri)
    {
        $currentUrlData = explode("/", $request->path());
        $host = Config::get('gateway.URL.' . strtoupper($currentUrlData[2]));
        $url = $host . $uri;
        $client = new Client();
        $language = $request->header('Accept-Language');
        try {
            return response()->json(
                $client->{$request->getMethod()}(
                    $url,
                    [
                        'form_params' => $request->all(),
                        'query' => $request->query(),
                        'headers' => ['Authorization' => $request->header('Authorization'), 'secretKey' => 'test', 'Accept-Language' => $language]
                    ]
                )->getBody()->__toString()
            );
        } catch (ClientException $e) {
            return response()->json($e->getResponse()->getBody()->__toString(), $e->getCode());
        } catch (\Throwable $e) {
            return response()->json($e->getResponse()->getBody()->__toString(), $e->getCode());
        }
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('gateway::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('gateway::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        return view('gateway::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
