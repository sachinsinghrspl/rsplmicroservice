<?php

return [
    'name' => 'Gateway',
    'URL' => [
        'ADMIN' => env('ADMIN_SERVICE_URL', 'http://127.0.0.1:80/api/')
    ]
];
