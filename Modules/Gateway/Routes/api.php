<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', function ($api) {
    $api->group([
        'namespace' => '\Modules\Gateway\Http\Controllers',
        'prefix' =>'gateway',
        // 'middleware' => ['auth:api']
    ], function($api) {
        $api->any('{uri}', 'InitController@index')->where('uri', '.*');
    });
});

// Route::middleware('auth:api')->get('/gateway', function (Request $request) {
//     return $request->user();
// });
