<?php

namespace App\Domains\User;


use App\Domains\User\GetCompanies;


class GetUsers implements GetCompanies,GetName
{

     public $userDetail =array();



/********
   Inteface function of
                  GetCompanies
*******/
    public function fetchCompanyDetail()
    {
      $this->userDetail['companyDetail'] = [
              ['current_company' => 'Tester software Pvt Ltd'],
               ['position' => 'Software Engineer']
         ];
    }

    /********    Inteface function of
                      GetName
    *******/

       public  function fetchNameDetail()
       {
         $this->userDetail['NameDetail'] = [
           ['first_name' => 'John'],
           ['last_name' => 'Doe'],
           ['address' => 'john Doe Address']
       ];
       }


    public function handle()
    {
      $this->fetchNameDetail();
      $this->fetchCompanyDetail();

      return $this->userDetail;
    }
}
