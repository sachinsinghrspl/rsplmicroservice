<?php

namespace App\Domains\Country\Jobs;

use Lucid\Foundation\Job;
use App\Domains\Country\GetCountry;
use App\Domains\Country\GetCountryName;

class CountryListJob extends Job implements GetCountry, GetCountryName
{
    public $countryData;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function getCountryData()
    {
        $this->countryData['countryData'] = [
            [
                'id' => 1,
                'name' => 'India',
                'code' => '50',
                'capital' => 'Delhi'
            ],
            [
                'id' => 2,
                'name' => 'Canada',
                'code' => '03',
                'capital' => 'Ottawa'
            ],
        ];
    }

    public function getCountryName()
    {
        $this->countryData['countryName'] = ['India', 'Canada'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->getCountryData();
        $this->getCountryName();
  
        return $this->countryData;
    }
}
