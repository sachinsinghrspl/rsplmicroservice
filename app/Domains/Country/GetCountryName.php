<?php

namespace App\Domains\Country;

interface GetCountryName
{
    public  function getCountryName();
}
