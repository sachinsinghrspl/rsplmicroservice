<?php

namespace App\Domains\Country;

interface GetCountry
{
    public  function getCountryData();
}
