<?php

namespace App\Features;

use Illuminate\Http\Request;
use Lucid\Foundation\Feature;
use App\Domains\Http\Jobs\RespondWithJsonJob;

class CountryFeature extends Feature
{
    public function handle(Request $request)
    {
        $users = $this->run(GetUsers::class);

        return $this->run(new RespondWithJsonJob($users));
    }
}
